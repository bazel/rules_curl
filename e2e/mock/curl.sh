#! /bin/sh

while test "${#}" -ne 0; do
  case "${1}" in
  *"runfiles/"*)
    printf '%s\n' "${1##*.runfiles/*/}"
    shift
    ;;
  "--upload-file")
    # Check that the input files exist
    printf '%s\n' "${1}"
    shift
    if ! test -e "${1}"; then
      printf >&2 'Not found: %s\n' "${1}"
      exit 2
    fi
    ;;
  *)
    printf '%s\n' "${1}"
    shift
    ;;
  esac
done
