load("@bazel_skylib//rules:common_settings.bzl", "string_flag")
load("@rules_curl//curl/upload/file:defs.bzl", "curl_upload_file")
load("@rules_diff//diff/file/test:defs.bzl", "diff_file_test")

genrule(
    name = "file_to_upload",
    outs = [
        "input.txt",
    ],
    cmd = "echo TEST > $@",
)

genrule(
    name = "file_to_upload_no_extension",
    outs = [
        "input",
    ],
    cmd = "echo TEST > $@",
)

curl_upload_file(
    name = "upload_basic",
    testonly = True,
    src = ":file_to_upload",
    url = "https://test.case/directory/fixture.txt",
)

curl_upload_file(
    name = "upload_no_extension",
    testonly = True,
    src = ":file_to_upload_no_extension",
    url = "https://test.case/directory/fixture",
)

string_flag(
    name = "injection",
    build_setting_default = "injection/",
    make_variable = "INJECT",
    visibility = ["//visibility:public"],
)

curl_upload_file(
    name = "upload_injection",
    testonly = True,
    src = ":file_to_upload",
    toolchains = [":injection"],
    url = "https://test.case/directory/$(INJECT)fixture.txt",
)

# We expect the url passed to curl to just be the url with a trailing
# slash, without a filename. That way curl will use the original file.
curl_upload_file(
    name = "upload_original_filename",
    testonly = True,
    src = ":file_to_upload",
    url = "https://test.case/directory/",
)

[
    (
        genrule(
            name = "execute_{}".format(test),
            testonly = True,
            outs = [
                "upload_{}.out".format(test),
            ],
            cmd = "./$(location :upload_{}) > $@".format(test),
            tools = [":upload_{}".format(test)],
        ),
        diff_file_test(
            name = "test_{}".format(test),
            size = "small",
            a = ":expected_{}.txt".format(test),
            b = ":execute_{}".format(test),
        ),
    )
    for test in ["basic", "original_filename", "injection", "no_extension"]
]

# Check that --output is forwarded on to curl binary.
genrule(
    name = "execute_with_output",
    testonly = True,
    outs = [
        "upload_with_output.out",
    ],
    cmd = "./$(location :upload_basic) --output output_file.txt > $@",
    tools = [":upload_basic"],
)

diff_file_test(
    name = "test_output",
    size = "small",
    a = ":expected_with_output.txt",
    b = ":upload_with_output.out"
)
