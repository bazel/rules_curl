package main

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/bazelbuild/rules_go/go/runfiles"
	"github.com/google/shlex"
)

type RunfileVar string

func (r RunfileVar) String() string {
	return string(r)
}

func (r *RunfileVar) Set(value string) error {
	runfile, err := runfiles.Rlocation(value)
	if err != nil {
		log.Fatal(err)
	}

	*r = RunfileVar(runfile)
	return nil
}

func upload(curl string, src string, output string, dest string, retry uint64, retry_delay uint64) error {
	args := []string{
		"--netrc",
		"--location",
		"--progress-bar",
		"--retry", strconv.FormatUint(retry, 10),
		"--retry-delay", strconv.FormatUint(retry_delay, 10),
		"--upload-file", src,
		"--url", dest,
	}

	if output != "" {
		args = append(args, "--output", output)
	}

	cmd := exec.Command(curl, args...)

	runfilesEnv, err := runfiles.Env()
	if err != nil {
		return err
	}

	cmd.Env = append(os.Environ(), runfilesEnv...)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		return err
	}

	return nil
}

func setArgs(args_file string) error {
	args_byt, err := os.ReadFile(args_file)
	if err != nil {
		return err
	}

	args := strings.Split(string(args_byt), "\n")
	// Remove ending newline
	args = args[:len(args)-1]

	for i, _ := range args {
		if i%2 != 0 {
			unquoted, err := shlex.Split(args[i])
			err = flag.Set(args[i-1], unquoted[0])
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func main() {
	var curl RunfileVar
	var retry uint64
	var retry_delay uint64
	var src RunfileVar
	var url string
	var output string

	flag.Var(&curl, "curl", "The path to the curl bin")
	flag.Uint64Var(&retry, "retry", 0, "The number of times to retry the request")
	flag.Uint64Var(&retry_delay, "retry-delay", 1, "The number of seconds to wait before retrying the request")

	flag.Var(&src, "src", "Path to the source file")
	flag.StringVar(&url, "url", "", "The URL to upload to")
	flag.StringVar(&output, "output", "", "Output file to use")

	args_file, err := runfiles.Rlocation("upload.args")
	if err != nil {
		log.Fatal(err)
	}

	err = setArgs(args_file)
	if err != nil {
		log.Fatal(err)
	}

	flag.Parse()
	err = upload(curl.String(), src.String(), output, url, retry, retry_delay)
	if err != nil {
		log.Fatal(err)
	}

}
