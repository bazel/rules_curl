visibility("//curl/...")

DOC = """Upload a file to a URL endpoint with cURL.

```py
file(
    name = "upload_file",
    src = ":data",
    url = "https://host.name.to.upload",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "File to be uploaded.",
        mandatory = True,
        allow_single_file = True,
    ),
    "url": attr.string(
        doc = "URL endpoint for file upload. Subject to 'Make variable' expansion.",
        mandatory = True,
    ),
    "retry": attr.int(
        doc = "The number of retry attempts.",
        default = 3,
    ),
    "retry_delay": attr.int(
        doc = "The seconds to wait before attempting a upload retry.",
        default = 1,
    ),
    "_upload": attr.label(
        default = "//curl/upload",
        allow_single_file = True,
        executable = True,
        cfg = "exec",
    ),
}

def _runfile(label, file):
    path = file.short_path
    if path.startswith("../"):
        return path.removeprefix("../")
    return "{}/{}".format(label.workspace_name or "_main", path)

def implementation(ctx):
    curl = ctx.toolchains["//curl/toolchain/curl:type"]

    if ctx.file.src.is_directory:
        fail("'src' must be a file not a directory.")

    # Do the 'Make variable' expansion on the URL.
    url = ctx.attr.url
    for k, v in ctx.var.items():
        url = url.replace("$({})".format(k), v)

    arguments = ctx.actions.declare_file("{}.args".format(ctx.label.name))
    args = ctx.actions.args()
    args.add("src", _runfile(ctx.file.src.owner, ctx.file.src))
    args.add("url", url)
    args.add("curl", _runfile(curl.executable.owner, curl.executable))
    args.add("retry", str(ctx.attr.retry))
    args.add("retry-delay", str(ctx.attr.retry_delay))
    ctx.actions.write(output = arguments, content = args)

    executable = ctx.actions.declare_file(ctx.label.name)
    ctx.actions.symlink(
        output = executable,
        target_file = ctx.executable._upload,
        is_executable = True,
    )

    files = depset([executable, arguments])
    root_symlinks = {"upload.args": arguments}

    runfiles = ctx.runfiles([ctx.executable._upload, curl.executable, ctx.file.src], root_symlinks = root_symlinks)
    runfiles = runfiles.merge(curl.default.default_runfiles)

    return DefaultInfo(
        executable = executable,
        files = files,
        runfiles = runfiles,
    )

file = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//curl/toolchain/curl:type"],
    executable = True,
)
