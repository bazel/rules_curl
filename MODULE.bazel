module(
    name = "rules_curl",
    bazel_compatibility = [
        ">=7.4.0",
    ],
)

bazel_dep(name = "bazel_skylib", version = "1.7.1")
bazel_dep(name = "toolchain_utils", version = "1.0.2")
bazel_dep(name = "ape", version = "1.0.1")
bazel_dep(name = "rules_go", version = "0.48.1")
bazel_dep(name = "gazelle", version = "0.36.0")

cosmos = use_extension("@ape//ape/cosmos:defs.bzl", "ape_cosmos")
use_repo(cosmos, cosmos = "curl")

go_deps = use_extension("@gazelle//:extensions.bzl", "go_deps")
go_deps.from_file(go_mod = "//curl/upload:go.mod")
use_repo(
    go_deps, "com_github_google_shlex")

go_sdk = use_extension("@rules_go//go:extensions.bzl", "go_sdk")
go_sdk.download(version = "1.23.4")

export = use_extension("@toolchain_utils//toolchain/export:defs.bzl", "toolchain_export")
export.symlink(
    name = "curl",
    target = "@cosmos",
)
use_repo(export, "curl")

resolved = use_repo_rule("@toolchain_utils//toolchain/resolved:defs.bzl", "toolchain_resolved")

resolved(
    name = "resolved-curl",
    toolchain_type = "//curl/toolchain/curl:type",
)

register_toolchains("//curl/toolchain/...")
