# `rules_curl`

> A Bazel ruleset for performing operations with cURL, hermetically.

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(module_name = "rules_curl", version = "0.0.0")
```

## Usage

### curl_upload_file

```py
load("@rules_curl//curl/upload/file:defs.bzl", "curl_upload_file")

curl_upload_file(
    name = "upload",
    src = ":fixture.txt",
    dst = "directory/fixture.txt",
    url = "https://test.case",
)

```
